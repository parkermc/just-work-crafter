package com.parkermc.jwcrafter;

import com.parkermc.jwcrafter.blocks.*;
import com.parkermc.jwcrafter.tileentity.TileEntityCrafter;

import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModBlocks {
	public static BlockCrafter crafter;
	
	public static void preInit() {
		GameRegistry.register(crafter = new BlockCrafter());
		GameRegistry.register(new ItemBlock(crafter), crafter.getRegistryName());
		GameRegistry.registerTileEntity(TileEntityCrafter.class, "tileEntityCrafter");
	}
	
	public static void initModels() {
		crafter.initModel();
	}
	
	public static void updateConfigData() {
	}

}

