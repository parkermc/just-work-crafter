package com.parkermc.jwcrafter.tileentity;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;

public class InventoryCrafterOut implements IInventory{
    private final List<ItemStack> stackResult = new ArrayList<ItemStack>();

    public InventoryCrafterOut() {
    	stackResult.add(ItemStack.EMPTY);
    }

    @Override
    public int getSizeInventory(){
        return 1;
    }
    
    @Override
    @Nullable
    public ItemStack getStackInSlot(int index){
        return this.stackResult.get(0);
    }

    @Override
    public String getName(){
        return "Result";
    }

    @Override
    public boolean hasCustomName(){
        return false;
    }

    @Override
    public ITextComponent getDisplayName(){
        return (ITextComponent)(this.hasCustomName() ? new TextComponentString(this.getName()) : new TextComponentTranslation(this.getName(), new Object[0]));
    }

    @Override
    @Nullable
    public ItemStack decrStackSize(int index, int count){
        return ItemStackHelper.getAndRemove(this.stackResult, 0);
    }

    @Override
    @Nullable
    public ItemStack removeStackFromSlot(int index){
        return ItemStackHelper.getAndRemove(this.stackResult, 0);
    }

    @Override
    public void setInventorySlotContents(int index, @Nullable ItemStack stack){
        this.stackResult.set(0, stack);
    }

    @Override
    public int getInventoryStackLimit(){
        return 64;
    }

    @Override
    public void markDirty(){
    }

    @Override
    public boolean isUsableByPlayer(EntityPlayer player){
        return true;
    }

    @Override
    public void openInventory(EntityPlayer player){}

    @Override
    public void closeInventory(EntityPlayer player){}

    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack){
        return true;
    }

    @Override
    public int getField(int id){
        return 0;
    }

    @Override
    public void setField(int id, int value){}

    @Override
    public int getFieldCount(){
        return 0;
    }

    @Override
    public void clear(){
        for (int i = 0; i < this.stackResult.size(); ++i){
            this.stackResult.set(i, ItemStack.EMPTY);
        }
    }

	@Override
	public boolean isEmpty() {
		return this.stackResult.get(0).isEmpty();
	}
}