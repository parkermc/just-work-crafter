package com.parkermc.jwcrafter.gui;

import com.parkermc.jwcrafter.tileentity.TileEntityCrafter;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryCraftResult;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerCrafter extends Container{
	private final TileEntityCrafter tile;
	
	private IInventory craftResult = new InventoryCraftResult();
	private int energy;
	private int tickAmount;
	
    public ContainerCrafter(InventoryPlayer playerInventory, TileEntityCrafter tile){  
        this.tile = tile;
        
        // Add the input slots
        for(int x = 0; x < 3; x++) {
        	for(int y = 0; y < 3; y++) {
        		this.addSlotToContainer(new Slot(this.tile.craftingInv, (y*3)+x, 26+(x*18), 17+(y*18)));
        	}
        }
        
        // Add the output slot
        this.addSlotToContainer(new Slot(this.tile.outInv, 9, 134, 35));
        
        // Add the result slot
        this.addSlotToContainer(new SlotCrafterResult(this.craftResult, 0, 98, 35));
        
        // add player inventory slots
        for (int i = 0; i < 3; ++i){
            for (int j = 0; j < 9; ++j){
            	this.addSlotToContainer(new Slot(playerInventory, j+i*9+9, 8+j*18, 84+i*18));
            }
        }
        
        // add hotbar slots
        for (int i = 0; i < 9; ++i){
        	this.addSlotToContainer(new Slot(playerInventory, i, 8 + i*18, 142));
        }
        
        this.updateResult();
    }
    
    public void updateResult() {
    	this.craftResult.setInventorySlotContents(0, (this.tile.recipe != null)?this.tile.recipe.getRecipeOutput().copy():ItemStack.EMPTY);
    }
    
    @Override
	public ItemStack slotClick(int slotId, int dragType, ClickType clickTypeIn, EntityPlayer player) {
		ItemStack item =  super.slotClick(slotId, dragType, clickTypeIn, player);
		this.updateResult();
		return item;
	}
	    
    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index){
    	ItemStack itemstack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);

        if (slot != null && slot.getHasStack()){
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if(index == 9) {
                if (!this.mergeItemStack(itemstack1, 11, 47, true)){
                    return ItemStack.EMPTY;
                }
                slot.onSlotChange(itemstack1, itemstack);
            }else if (index == 10){
            	return ItemStack.EMPTY;
            }else if (index > 10){
                if (!this.mergeItemStack(itemstack1, 0, 9, false)){ // Move to crafting
                	if (index >= 11 && index < 38){
                		if (!this.mergeItemStack(itemstack1, 38, 47, false)){
                			return ItemStack.EMPTY;
                		}
                	}else if (index >= 38 && index < 47 && !this.mergeItemStack(itemstack1, 11, 38, false)){
                		return ItemStack.EMPTY;
                	}
                }
            }else if (!this.mergeItemStack(itemstack1, 11, 47, false)){
                return ItemStack.EMPTY;
            }

            if (!itemstack1.isEmpty()){
                slot.onSlotChanged();
            }

            if (itemstack1.getCount() == itemstack.getCount()){
                return ItemStack.EMPTY;
            }

            slot.onTake(playerIn, itemstack1);
        }

        return itemstack;
    }
    
    @Override
    public boolean canMergeSlot(ItemStack stack, Slot slotIn) {
    	return (slotIn.slotNumber == 9||slotIn.slotNumber == 10)?false:super.canMergeSlot(stack, slotIn);
    }
    
	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return tile.isUsableByPlayer(playerIn);
	}
	
	@Override
	public void addListener(IContainerListener listener) {
		super.addListener(listener);
		listener.sendAllWindowProperties(this, this.tile);
	}
	
	@Override
	public void updateProgressBar(int id, int data) {
		this.tile.setField(id, data);
	}
	
	@Override
	public void putStackInSlot(int slotID, ItemStack stack) {
		super.putStackInSlot(slotID, stack);
		this.updateResult();
	}
	
	@Override
	public void detectAndSendChanges() {
		super.detectAndSendChanges();
		
		for (int i = 0; i < this.listeners.size(); ++i){
            IContainerListener icontainerlistener = (IContainerListener)this.listeners.get(i);

            if (this.energy != this.tile.getField(0)){
                icontainerlistener.sendProgressBarUpdate(this, 0, this.tile.getField(0));
            }

            if (this.tickAmount != this.tile.getField(1)){
                icontainerlistener.sendProgressBarUpdate(this, 1, this.tile.getField(1));
            }
        }
        this.energy = this.tile.getField(0);
        this.tickAmount = this.tile.getField(1);
	}
}
