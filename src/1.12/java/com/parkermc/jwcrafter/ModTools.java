package com.parkermc.jwcrafter;

import java.util.List;

import javax.annotation.Nonnull;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.management.PlayerList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModTools {
	public static void logError(String format, Object... data) {
		FMLLog.log.error(format, data);
	}
	
	public static void addShapedRecipe(String name, @Nonnull ItemStack output, Object... params) {
		GameRegistry.addShapedRecipe(new ResourceLocation(ModMain.MODID, name), null, output, params);
	}
	
	public static List<EntityPlayerMP> getPlayers(PlayerList list){
		return list.getPlayers();
	}
}
