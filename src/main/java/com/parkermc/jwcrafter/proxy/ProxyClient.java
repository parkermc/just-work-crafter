package com.parkermc.jwcrafter.proxy;

import com.parkermc.jwcrafter.ModBlocks;
import com.parkermc.jwcrafter.events.EventHandlerClient;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class ProxyClient extends ProxyCommon {
	public static Configuration serverConfig = null;
	private EventHandlerClient eventHandlerConnect = new EventHandlerClient();
	
	public void preInit(FMLPreInitializationEvent event) {
		super.preInit(event);
		MinecraftForge.EVENT_BUS.register(eventHandlerConnect);
		ModBlocks.initModels();
	}
	
	public void init(FMLInitializationEvent event) {
		super.init(event);
	}
	
	public void postInit(FMLPostInitializationEvent event) {
		super.postInit(event);
	}

}
