package com.parkermc.jwcrafter.tileentity;

import com.parkermc.jwcrafter.ModConfig;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class TileEntityCrafter extends TileEntity implements ISidedInventory, ITickable, IEnergyStorage{
    private static final int[] slotsTop = new int[] {0,1,2,3,4,5,6,7,8};
    private static final int[] slotsBottom = new int[] {9};
	private static final int[] slotsSides = new int[] {};
	
	@CapabilityInject(IEnergyStorage.class)
	static Capability<IEnergyStorage> ENERGY_HANDLER_CAPABILITY = null;

	private String customName;
	
	public InventoryCrafterOut outInv = new InventoryCrafterOut();
	public InventoryCrafting craftingInv = new InventoryCrafting(new Container() {public boolean canInteractWith(EntityPlayer playerIn) {return false;}public void onCraftMatrixChanged(net.minecraft.inventory.IInventory inventoryIn) {updateRecipe();};}, 3, 3);
	
    protected int energy = 0;

	private int tickAmount = 0;
	
	public IRecipe recipe = null;
	      
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
		if(capability == CapabilityEnergy.ENERGY) {
			return true;
		}
		return super.hasCapability(capability, facing);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
		if(capability == CapabilityEnergy.ENERGY) {
			return (T)this;
		}
		return super.getCapability(capability, facing);
	}
	
    @Override
    public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate) {
    	return false;
    }
    
	@Override
	public int getSizeInventory() {
		return 10;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		return (index == 9) ? this.outInv.getStackInSlot(0) : this.craftingInv.getStackInSlot(index);
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		return (index == 9) ? this.outInv.decrStackSize(0, count) : this.craftingInv.decrStackSize(index, count);
	}
	
	@Override
	public ItemStack removeStackFromSlot(int index) {
		return (index == 9) ? this.outInv.removeStackFromSlot(0) : this.craftingInv.removeStackFromSlot(index);
	}
	
    @Override
    public void readFromNBT(NBTTagCompound compound){
    	super.readFromNBT(compound);
    	NBTTagList nbttaglist = compound.getTagList("Items", 10);
    	this.clear();

    	for (int i = 0; i < nbttaglist.tagCount(); ++i){
    		NBTTagCompound nbtTagCompound = nbttaglist.getCompoundTagAt(i);
    		byte b0 = nbtTagCompound.getByte("Slot");
    		if (b0 >= 0 && b0 < this.getSizeInventory()){
    			this.setInventorySlotContents(b0, new ItemStack(nbtTagCompound));
    		}
    	}

    	if (compound.hasKey("CustomName", 8)){
    		this.customName = compound.getString("CustomName");
    	}
    	this.tickAmount = compound.getInteger("tick_amount");
    	this.energy = compound.getInteger("energy");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound){
    	compound = super.writeToNBT(compound);
    	NBTTagList nbtSlots = new NBTTagList();

    	for (int i = 0; i < this.getSizeInventory(); ++i){
    		if (this.getStackInSlot(i) != null){
    			NBTTagCompound nbtTagCompound = new NBTTagCompound();
    			nbtTagCompound.setByte("Slot", (byte)i);
    			this.getStackInSlot(i).writeToNBT(nbtTagCompound);
    			nbtSlots.appendTag(nbtTagCompound);
    		}
    	}
    	compound.setTag("Items", nbtSlots);

    	if (this.hasCustomName()){
    		compound.setString("CustomName", this.customName);
		}
    	
    	compound.setInteger("tick_amount", this.tickAmount);
    	compound.setInteger("energy", this.energy);
    	return compound;
    }
    
    @Override
    public ITextComponent getDisplayName() {
    	return (this.hasCustomName())? new TextComponentString(this.getName()) : new TextComponentTranslation(this.getName());
    }

	@Override
	public void setInventorySlotContents(int index, ItemStack stack)     {
		if(index == 9) {	
			this.outInv.setInventorySlotContents(0, stack);
		}else {
			this.craftingInv.setInventorySlotContents(index, stack);
		}
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}
	
	@Override
	public void openInventory(EntityPlayer player) {
	}

	@Override
	public void closeInventory(EntityPlayer player) {
	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		return (index == 9) ? this.outInv.isItemValidForSlot(0, stack): this.craftingInv.isItemValidForSlot(index, stack);
	}

	public int getEnergyPercent() {
		return (this.energy * 100) / ModConfig.EnergyCapacity;
	}
	
	public int getProgressPercent() {
		return (this.tickAmount * 100) / ModConfig.CraftTicks;
	}
	
	
	@Override
	public int getField(int id) {
		switch(id) {
		case 0:
			return this.energy;
		case 1:
			return this.tickAmount;
		default:
			return 0;
		}
	}

	@Override
	public void setField(int id, int value) {
		switch(id) {
		case 0:
			this.energy = value;
			break;
		case 1:
			this.tickAmount = value;
			break;
		}
	}

	@Override
	public int getFieldCount() {
		return 2;
	}

	@Override
	public void clear() {
		this.craftingInv.clear();
		this.outInv.clear();
	}

	@Override
	public String getName() {
		return this.hasCustomName() ? this.customName : "container.crafter";
	}

	@Override
	public boolean hasCustomName() {
		return this.customName != null && this.customName.length() > 0;
	}

	@Override
	public int[] getSlotsForFace(EnumFacing side) {
		return side == EnumFacing.DOWN ? slotsBottom : (side == EnumFacing.UP ? slotsTop : slotsSides);
	}

	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction) {
		return this.isItemValidForSlot(index, itemStackIn);
	}

	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction) {
		return true;
	}

	@Override
	public void update() {
		if(recipe != null&&(!this.world.isRemote)&&this.energy >= ModConfig.EnergyPerTick) {
			if(this.tickAmount <= ModConfig.CraftTicks) {
				this.tickAmount++;
				this.energy -= ModConfig.EnergyPerTick;
			}else if(this.outInv.getStackInSlot(0).isEmpty() ||
					(this.recipe.getCraftingResult(this.craftingInv).isItemEqual(this.outInv.getStackInSlot(0)) && this.outInv.getStackInSlot(0).getCount()+this.recipe.getCraftingResult(this.craftingInv).getCount() <= this.outInv.getStackInSlot(0).getMaxStackSize())) {
				this.tickAmount = 0;
				if(this.outInv.getStackInSlot(0).isEmpty()) {
					this.outInv.setInventorySlotContents(0, this.recipe.getCraftingResult(this.craftingInv).copy());
				}else {
					ItemStack stack = this.outInv.getStackInSlot(0);
					stack.setCount(stack.getCount() + this.recipe.getCraftingResult(this.craftingInv).getCount());
					this.outInv.setInventorySlotContents(0, stack);
				}
				NonNullList<ItemStack> remainingItems = this.recipe.getRemainingItems(craftingInv);
		        for (int i = 0; i < remainingItems.size(); ++i){
		            ItemStack itemstack = this.craftingInv.getStackInSlot(i);
		            ItemStack itemstack1 = remainingItems.get(i);

		            if (!itemstack.isEmpty()){
		                this.craftingInv.decrStackSize(i, 1);
		                itemstack = this.craftingInv.getStackInSlot(i);
		            }

		            if (!itemstack1.isEmpty()){
		                if (itemstack.isEmpty()){
		                    this.craftingInv.setInventorySlotContents(i, itemstack1);
		                }else if (ItemStack.areItemsEqual(itemstack, itemstack1) && ItemStack.areItemStackTagsEqual(itemstack, itemstack1)){
		                    itemstack1.setCount(itemstack1.getCount() + itemstack.getCount());
		                    this.craftingInv.setInventorySlotContents(i, itemstack1);
		                }else{
		                	this.world.spawnEntity(new EntityItem(this.world, this.getPos().getX(), this.getPos().getY(), this.getPos().getZ(), itemstack1));
		                }
		            }
		        }
		        this.updateRecipe();
			}
			this.markDirty();
		}
	}
	
	public void updateRecipe() {
		if(this.recipe != null&&this.recipe.matches(craftingInv, this.world)) {
			return;
		}
		this.recipe = null;
		this.tickAmount = 0;
		for(IRecipe recipe : ForgeRegistries.RECIPES.getValues()) {
			if(recipe.matches(craftingInv, this.world)) {
				this.recipe = recipe;
				return;
			}
		}
	}
    @Override
    public int receiveEnergy(int maxReceive, boolean simulate)
    {
        if (!canReceive()) {
            return 0;
        }
        int energyReceived = Math.min(ModConfig.EnergyCapacity - energy, Math.min(ModConfig.EnergyInputRate, maxReceive));
        if (!simulate) {
            energy += energyReceived;
            this.markDirty();
        }
        return energyReceived;
    }

    @Override
    public int extractEnergy(int maxExtract, boolean simulate)
    {
        if (!canExtract()) {
            return 0;
        }

        int energyExtracted = Math.min(energy, Math.min(ModConfig.EnergyOutputRate, maxExtract));
        if (!simulate) {
            energy -= energyExtracted;
            this.markDirty();
        }
        return energyExtracted;
    }

    @Override
    public int getEnergyStored(){
        return energy;
    }

    @Override
    public int getMaxEnergyStored(){
        return ModConfig.EnergyCapacity;
    }

    @Override
    public boolean canExtract(){
        return ModConfig.EnergyOutputRate > 0;
    }

    @Override
    public boolean canReceive(){
        return ModConfig.EnergyInputRate > 0;
    }

	@Override
	public boolean isEmpty() {
		return (this.outInv.isEmpty()&&this.craftingInv.isEmpty());
	}

	@Override
	public boolean isUsableByPlayer(EntityPlayer player) {
		return true;
	}
}
