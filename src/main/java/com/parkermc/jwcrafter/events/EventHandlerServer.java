package com.parkermc.jwcrafter.events;

import com.parkermc.jwcrafter.ModMain;
import com.parkermc.jwcrafter.network.MessageConfig;
import com.parkermc.jwcrafter.proxy.ProxyCommon;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;

public class EventHandlerServer {
	
	@SubscribeEvent
	public void onConnect(PlayerLoggedInEvent event) {
		ModMain.network.sendTo(new MessageConfig(ProxyCommon.config), (EntityPlayerMP)event.player);
	}
}
