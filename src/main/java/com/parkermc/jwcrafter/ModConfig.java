package com.parkermc.jwcrafter;

import com.parkermc.jwcrafter.proxy.ProxyClient;
import com.parkermc.jwcrafter.proxy.ProxyCommon;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ModConfig {
	private static final String CATEGORY_GENERAL = "general";
	public static int CraftTicks = 80;
	public static int EnergyPerTick = 1;
	public static int EnergyInputRate = 20;
	public static int EnergyOutputRate = 20;
	public static int EnergyCapacity = 1000;
	
	
    public static void readConfig() {
        Configuration cfg = ProxyCommon.config;
        try {
            cfg.load();
            initGeneralConfig(cfg);
        } catch (Exception e1) {
            ModTools.logError("Problem loading config file!", e1);
        } finally {
            if (cfg.hasChanged()) {
                cfg.save();
                cfg.load();
            }
        } 
        if(ProxyClient.serverConfig != null){
        	initGeneralConfig(ProxyClient.serverConfig);
        }
    }
    
    private static void initGeneralConfig(Configuration cfg) {
        cfg.addCustomCategoryComment(CATEGORY_GENERAL, "General configuration");
        CraftTicks = cfg.getInt("craft_ticks", CATEGORY_GENERAL, CraftTicks, 1, 99999, "The amount of ticks for each craft to happen");
        EnergyPerTick = cfg.getInt("energy_per_tick", CATEGORY_GENERAL, EnergyPerTick, 0, 999, "");
        EnergyInputRate = cfg.getInt("energy_input_rate", CATEGORY_GENERAL, EnergyInputRate, -1, 9999, "");
        EnergyOutputRate = cfg.getInt("energy_output_rate", CATEGORY_GENERAL, EnergyOutputRate, -1, 9999, "");
        EnergyCapacity = cfg.getInt("energy_capacity", CATEGORY_GENERAL, EnergyCapacity, 1, 9999, "");
    }
        
    @Mod.EventBusSubscriber
	static class ConfigurationHandler {
		@SubscribeEvent
		public static void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event) {
			if (event.getModID().equals(ModMain.MODID) && ProxyCommon.config.hasChanged()) {
				ProxyCommon.config.save();
				readConfig();
			}
		}
	}
}
