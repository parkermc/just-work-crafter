package com.parkermc.jwcrafter.events;

import com.parkermc.jwcrafter.ModConfig;
import com.parkermc.jwcrafter.proxy.ProxyClient;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent.ClientDisconnectionFromServerEvent;

public class EventHandlerClient {
	
	@SubscribeEvent
	public void onDisconnect(ClientDisconnectionFromServerEvent event) {
		ProxyClient.serverConfig = null;
		ModConfig.readConfig();
	}
}
