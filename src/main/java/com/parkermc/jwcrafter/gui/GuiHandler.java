package com.parkermc.jwcrafter.gui;

import com.parkermc.jwcrafter.ModMain;
import com.parkermc.jwcrafter.tileentity.TileEntityCrafter;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class GuiHandler implements IGuiHandler{

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, 
          World world, int x, int y, int z) 
    { 
        TileEntity tileEntity = world.getTileEntity(new BlockPos(x, y, z));

        if (tileEntity != null)
        {
            if (ID == ModMain.GUI_ENUM.CRAFTER.ordinal())
            {
                return new ContainerCrafter(player.inventory, (TileEntityCrafter)tileEntity);
            }
        }

        return null;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z){
        TileEntity tileEntity = world.getTileEntity(new BlockPos(x, y, z));

        if (tileEntity != null){
            if (ID == ModMain.GUI_ENUM.CRAFTER.ordinal()){
                return new GuiCrafter(player.inventory, (TileEntityCrafter)tileEntity);
            }
        }
        return null;
    }
}