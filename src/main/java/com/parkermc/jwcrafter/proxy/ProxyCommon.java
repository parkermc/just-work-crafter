package com.parkermc.jwcrafter.proxy;

import java.io.File;

import com.parkermc.jwcrafter.ModBlocks;
import com.parkermc.jwcrafter.ModConfig;
import com.parkermc.jwcrafter.ModMain;
import com.parkermc.jwcrafter.ModRecipes;
import com.parkermc.jwcrafter.gui.GuiHandler;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;

public class ProxyCommon {
	public static Configuration config;
	
	public void preInit(FMLPreInitializationEvent event) {
		File directory = event.getModConfigurationDirectory();
        config = new Configuration(new File(directory.getPath(), ModMain.MODID+".cfg"));
        ModConfig.readConfig();
		ModBlocks.preInit();
		ModRecipes.preInit();
	}
	
	public void init(FMLInitializationEvent event) {
		NetworkRegistry.INSTANCE.registerGuiHandler(ModMain.instance, new GuiHandler());
	}
	
	public void postInit(FMLPostInitializationEvent event) {
	}
}
