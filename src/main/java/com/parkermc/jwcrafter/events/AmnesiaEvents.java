package com.parkermc.jwcrafter.events;

import com.parkermc.amnesia.events.AmnesiaEvent;
import com.parkermc.amnesia.events.IAmnesiaEvents;
import com.parkermc.jwcrafter.ModTools;
import com.parkermc.jwcrafter.gui.ContainerCrafter;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.fml.common.FMLCommonHandler;

@AmnesiaEvent
public class AmnesiaEvents implements IAmnesiaEvents{
	
	@Override
	public void normal() {
	}

	@Override
	public void random() {
	}

	@Override
	public void updatePost() {
		if(!DimensionManager.getWorld(0).isRemote) {
			for(EntityPlayerMP player : ModTools.getPlayers((FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList()))) {
				if(player.openContainer instanceof ContainerCrafter) {
					((ContainerCrafter)player.openContainer).updateResult();
				}
			}
		}
	}

	@Override
	public void updatePostClient() {
	}

	@Override
	public void cure() {
	}
}