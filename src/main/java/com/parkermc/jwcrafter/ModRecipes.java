package com.parkermc.jwcrafter;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class ModRecipes {
	
	public static void preInit() {
		ModTools.addShapedRecipe("crafter", new ItemStack(ModBlocks.crafter),
			" D ",
			"PCP",
			"RRR",
			'C', Blocks.CRAFTING_TABLE,
			'D', Items.DIAMOND,
			'P', Blocks.PISTON,
			'R', Items.REDSTONE
		);
	}
}
