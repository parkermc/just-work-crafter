package com.parkermc.jwcrafter;

import com.parkermc.jwcrafter.blocks.*;
import com.parkermc.jwcrafter.tileentity.TileEntityCrafter;

import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModBlocks {
	public static BlockCrafter crafter;
	
	public static void preInit() {
		ForgeRegistries.BLOCKS.register(crafter = new BlockCrafter());
		ForgeRegistries.ITEMS.register(new ItemBlock(crafter).setRegistryName(crafter.getRegistryName()));
		GameRegistry.registerTileEntity(TileEntityCrafter.class, "tileEntityCrafter");
	}
	
	public static void initModels() {
		crafter.initModel();
	}
	
	public static void updateConfigData() {
	}

}

