package com.parkermc.jwcrafter;

import com.parkermc.jwcrafter.network.MessageConfig;
import com.parkermc.jwcrafter.proxy.ProxyCommon;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = ModMain.MODID, name = ModMain.MODNAME, version = ModMain.VERSION, guiFactory = "com.parkermc.jwcrafter.gui.GuiModConfigFactory")
public class ModMain{
    public static final String MODID = "jwcrafter";
    public static final String MODNAME = "Just work crafter";
    public static final String VERSION = "1.0.0";
    
    public enum GUI_ENUM {
        CRAFTER
    }
    
    @SidedProxy(clientSide = "com.parkermc.jwcrafter.proxy.ProxyClient", serverSide = "com.parkermc.jwcrafter.proxy.ProxyServer", modId = MODID)
    public static ProxyCommon proxy;
    
    @Instance(MODID)
    public static ModMain instance;
    
    public static SimpleNetworkWrapper network; 
    public static ModTab tab = new ModTab("jwcrafter");
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event){
    	proxy.preInit(event);
    	network = NetworkRegistry.INSTANCE.newSimpleChannel(MODID);
    	network.registerMessage(MessageConfig.Handler.class, MessageConfig.class, 0, Side.CLIENT);
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
    	proxy.init(event);    
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
    	proxy.postInit(event);
    }
}

