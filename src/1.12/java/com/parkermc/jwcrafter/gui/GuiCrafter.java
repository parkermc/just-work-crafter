package com.parkermc.jwcrafter.gui;

import com.parkermc.jwcrafter.ModMain;
import com.parkermc.jwcrafter.tileentity.TileEntityCrafter;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiCrafter extends GuiContainer{
	
	    private final ResourceLocation guiTexture = new ResourceLocation(ModMain.MODID +":textures/gui/crafter.png");
	    private final InventoryPlayer playerInventory;
	    private final TileEntityCrafter tile;

	    public GuiCrafter(InventoryPlayer playerInventory, TileEntityCrafter tile){
	        super(new ContainerCrafter(playerInventory, tile));
	        this.playerInventory = playerInventory;
	        this.tile = tile;
	    }

	    @Override
	    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY){
	        String s = this.tile.getDisplayName().getUnformattedText();
	        this.fontRenderer.drawString(s, this.xSize/2-this.fontRenderer.getStringWidth(s)/2, 6, 4210752);
	        
	        this.fontRenderer.drawString(this.playerInventory.getDisplayName().getUnformattedText(), 8, this.ySize - 94, 4210752);
	        
	        String powerPercent = String.valueOf(this.tile.getEnergyPercent())+"%";
	        this.fontRenderer.drawString(powerPercent, 15-(this.fontRenderer.getStringWidth(powerPercent)/2), 7, 4210752);
	    }

	    @Override
	    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY){
	        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
	        mc.getTextureManager().bindTexture(this.guiTexture);
	        
	        int marginHorizontal = (this.width - this.xSize) / 2;
	        int marginVertical = (this.height - this.ySize) / 2;
	        
	        this.drawTexturedModalRect(marginHorizontal, marginVertical, 0, 0, this.xSize, this.ySize);
	        
	        int powerPercent = this.tile.getEnergyPercent();
	        this.drawTexturedModalRect(marginHorizontal+7, marginVertical+68-(powerPercent/2), this.xSize, 17, 8, powerPercent/2);
	        
	        int jobPercent = this.tile.getProgressPercent();
	        this.drawTexturedModalRect(marginHorizontal+94, marginVertical+59, this.xSize, 0, jobPercent/4, 17);
	    }
	 }
